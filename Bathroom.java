/* 
 * A concrete room
 * A Bathroom 
 * */

public class Bathroom implements IRoom{
    private String name;

    public Bathroom(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public void accept(IRoomVisitor visitor){
        visitor.visit(this);
    }
}
