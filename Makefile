CC 		:= javac
VM 		:= java
OPTS 	:=
SRC 	:= FloorVisitDemo.java Bathroom.java BuildingFloor.java ConferenceRoom.java IRoom.java IRoomVisitor.java LectureTheatre.java Office.java RoomCleanVisitor.java RoomNamePrintVisitor.java
OBJ 	:= $(patsubst %.java,%.class,${SRC})
EXE 	:= $(basename ${SRC})

all: demo 

demo: 
	${CC} ${OPTS} ${SRC}

run:
	${VM} ${EXE} 

clean:
	@rm -rf ${OBJ} 
