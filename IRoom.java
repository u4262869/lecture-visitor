/* 
 * Room Interface
 * This is an element that a visitor might visit
 */

public interface IRoom{
    void accept(IRoomVisitor visitor);
}
