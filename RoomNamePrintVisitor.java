/* 
 * A concrete visitor
 * This visitor prints the name of the room it visits
 * */

public class RoomNamePrintVisitor implements IRoomVisitor{
    
    public void visit(ConferenceRoom room){
        System.out.println("Visiting Conference Room: " + room.getName());
    }
    public void visit(LectureTheatre room){
        System.out.println("Visiting Lecture Theatre: " + room.getName());
    }
    public void visit(Office room){
        System.out.println("Visiting Office : " + room.getName());
    }
    public void visit(Bathroom room){
        System.out.println("Visiting Bathroom: " + room.getName());
    }
    public void visit(BuildingFloor room){
        System.out.println("Visiting Building Floor");
    }
}
