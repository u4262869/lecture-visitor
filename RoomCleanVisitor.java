/* 
 * A concrete visitor
 * This visitor cleans each room it visits
 * */

public class RoomCleanVisitor implements IRoomVisitor{
    
    public void visit(ConferenceRoom room){
        System.out.println("Cleaning Conference Room: " + room.getName());
    }
    public void visit(LectureTheatre room){
        System.out.println("Cleaning Lecture Theatre: " + room.getName());
    }
    public void visit(Office room){
        System.out.println("Cleaning Office : " + room.getName());
    }
    public void visit(Bathroom room){
        System.out.println("Cleaning Bathroom: " + room.getName());
    }
    public void visit(BuildingFloor room){
        System.out.println("Cleaning Building Floor");
    }
}
