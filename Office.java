/* 
 * A concrete room
 * An Office Room 
 * */

public class Office implements IRoom{
    private String name;

    public Office(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public void accept(IRoomVisitor visitor){
        visitor.visit(this);
    }
}
