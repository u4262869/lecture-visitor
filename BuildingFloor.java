/* 
 * A concrete room
 * A floor in the building which has multiple rooms
 * */

public class BuildingFloor implements IRoom{
    private IRoom[] rooms;

    public BuildingFloor(){
        this.rooms = new IRoom[]{
                            new Office("Front Office"),
                            new Office("Managers Office"),
                            new Office("Directors Office"),
                            new Bathroom("Common bathroom"),
                            new ConferenceRoom("Boardroom 1"),
                            new ConferenceRoom("Boardroom 2"),
                            new LectureTheatre("Presentation room 1"),
                            new LectureTheatre("Presentation room 2")
                        };
    }

    public void accept(IRoomVisitor visitor){
        for (IRoom room : rooms){
            room.accept(visitor);
        }
        visitor.visit(this);
    }
}
