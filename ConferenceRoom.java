/* 
 * A concrete room
 * A Conference Room 
 * */

public class ConferenceRoom implements IRoom{
    private String name;

    public ConferenceRoom(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public void accept(IRoomVisitor visitor){
        visitor.visit(this);
    }
}
