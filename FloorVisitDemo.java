/* 
 * A Demonstration of the visitor pattern
 * */

public class FloorVisitDemo{

    public static void main(String[] args){
        IRoom floor = new BuildingFloor();
        floor.accept(new RoomNamePrintVisitor());
        floor.accept(new RoomCleanVisitor());
    }
}
