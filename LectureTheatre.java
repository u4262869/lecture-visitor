/* 
 * A concrete room
 * A Lecture theatre 
 * */

public class LectureTheatre implements IRoom{
    private String name;

    public LectureTheatre(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public void accept(IRoomVisitor visitor){
        visitor.visit(this);
    }
}
