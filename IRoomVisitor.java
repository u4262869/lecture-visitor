/* 
 * Room visitor interface
 * This defines a visitor
 * */

public interface IRoomVisitor{
    void visit(ConferenceRoom room);
    void visit(LectureTheatre room);
    void visit(Office room);
    void visit(Bathroom room);
    void visit(BuildingFloor room);
}
